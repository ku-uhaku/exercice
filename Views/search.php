<?php
$URL_ROOT = "../";
    require "begin.php";
    require_once $URL_ROOT.'Models/model.php';
    ?>
    <h1> Find Among Nobles prizes </h1>
    <?php
    $Obj = new Model();
    if(isset($_GET['search'])){
        $name = $_GET['name'];
        $year = $_GET['year'];
        $SignYear = $_GET['SignYear'];
        $filters = array(
            "name" => $name,
            "year" => $year,
            "SignYear" => $SignYear,
        ); 
        $users = $Obj->search_nobel_prizes($filters, 0, 25);
        if(count($users) === 1) {
            header("Location: information.php?id=" . $users[0]['id']);
        }
        else{
            ?>
            <table border=1>
                <tr>
                    <th>Year</th>
                    <th>Category</th>
                    <th>Name</th>
                    <th>birthdate</th>
                    <th>birthplace</th>
                    <th>county</th>
                </tr>
                <?php foreach($users as $user): ?>
                <tr>
                    <td><?= $user['year'] ?></td>
                    <td><?= $user['category'] ?></td>
                    <td><?= $user['name'] ?></td>
                    <td><?= $user['birthdate'] ?></td>
                    <td><?= $user['birthplace'] ?></td>
                    <td><?= $user['county'] ?></td>
                </tr>
           
            <?php endforeach; ?>
            </table>
             <?php
        }
       
        
       
    }
    require "end.php"
?>