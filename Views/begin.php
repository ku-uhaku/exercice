<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8"/>
		<title> Nobel Prizes</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="<?=  $URL_ROOT; ?>Content/css/nobel.css"/>
		
	</head>
	<body>
		
		<nav>
			<ul>
				<li><a href="<?=  $URL_ROOT; ?>Views/last25.php"> Last Nobel Prizes</a></li>
				<li><a href="<?=  $URL_ROOT; ?>Views/form_add.php"> Add a Nobel prize</a></li>
			</ul>
		</nav>

		<header>
			<h1><a href="<?=  $URL_ROOT; ?>/home.php"> Nobel Prizes </a></h1>
		</header>

		<main>